BIN = node_modules/.bin

build:
	$(BIN)/browserify -t babelify -o dist/bundle.js src/client/app.js
	$(BIN)/stylus -o dist/bundle.css src/client/app.styl

watch:
	$(BIN)/watchify -d -v -t babelify -o dist/bundle.js src/client/app.js & \
	$(BIN)/stylus -w -o dist/bundle.css src/client/app.styl & \
	node -r babel-register src/server/app.js & \
	wait

test: flow eslint coverage

flow:
	$(BIN)/flow

eslint:
	$(BIN)/eslint src

coverage:
	$(BIN)/nyc --all --reporter text-summary --reporter html $(BIN)/mocha --require babel-register 'src/**/*.test.js'

e2e:
	$(BIN)/mocha -t 0 --require babel-register 'src/e2e.js'

format:
	$(BIN)/eslint --fix src

deploy: build
	git checkout --detach
	git add -f dist/bundle.css dist/bundle.js
	git commit -m 'chore: deploy to heroku'
	git tag -f heroku
	git push --force-with-lease heroku heroku:master

.PHONY: test build watch format deploy coverage flow eslint e2e
