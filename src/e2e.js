/* eslint-env mocha, browser */
import puppeteer from 'puppeteer';
import assert from 'assert';

let browser;

describe('the app', () => {
  it('should work', async () => {
    browser = await puppeteer.launch({
      args: ['--no-sandbox'],
      headless: true, // set to false if you want to know what's happening
    });
    const page = await browser.newPage();

    // load the page
    await page.goto('http://localhost:3000');
    await page.waitForNavigation({waitUntil: 'networkidle'});
    await page.screenshot({path: 'test-01.png'});

    // wait for initial search results
    await page.waitForSelector('.prezi');
    await page.waitForNavigation({waitUntil: 'networkidle'});
    await page.screenshot({path: 'test-02.png'});
    assert.equal((await page.$$('.prezi')).length, 12);

    // load another page
    await page.click('.prezi-list--item--load-more button');
    await page.waitForSelector('.prezi-list--item--loading');
    await page.waitForNavigation({waitUntil: 'networkidle'});
    assert.equal((await page.$$('.prezi')).length, 24);

    // test search
    await page.type('.prezi-search input', 'enim');
    await page.waitForSelector('.prezi-list--item--loading');
    await page.waitForNavigation({waitUntil: 'networkidle'});
    await page.screenshot({path: 'test-03.png'});
    assert.equal((await page.$$('.prezi')).length, 3);
    assert.equal(await page.evaluate(() => document.querySelector('.prezi--date').innerText), 'March 3, 2016');

    // change sort order
    await page.click('.prezi-search button');
    await page.waitForNavigation({waitUntil: 'networkidle'});
    await page.screenshot({path: 'test-04.png'});
    assert.equal(await page.evaluate(() => document.querySelector('.prezi--date').innerText), 'July 11, 2015');
  });

  after(() => browser.close());
});
