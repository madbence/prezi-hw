/* eslint-env mocha */

import proxyquire from 'proxyquire';
import assert from 'assert';
import {parse} from 'url';

const mock = fetch => proxyquire('./api', {
  './fetch': {
    '__esModule': true,
    '@noCallThru': true,
    'default': fetch,
  },
});

const response = (res, status = 200) => Promise.resolve({
  status,
  json: () => Promise.resolve(res),
});

const expectRejection = p => p.then(() => assert.fail('Rejection was expected'), () => {});

describe('client/api', () => {
  it('should query the right url', () => {
    const api = mock(query => {
      const url = parse(query, true);
      assert.equal(url.query.q, 'foo');
      assert.equal(url.query.order, 'asc');
      assert.equal(url.query.page, '0');
      assert.equal(url.pathname, '/search');
      return response([]);
    });
    return api.search('foo', 'asc', 0);
  });
  it('should throw on 4xx', () => {
    const api = mock(() => response([], 404));
    return expectRejection(api.search('foo', 'asc', 0));
  });
  it('should throw on 5xx', () => {
    const api = mock(() => response([], 404));
    return expectRejection(api.search('foo', 'asc', 0));
  });
  it('should throw on network error', () => {
    const api = mock(() => Promise.reject(new TypeError('Network error!')));
    return expectRejection(api.search('foo', 'asc', 0));
  });
  it('should return the results', async () => {
    const api = mock(() => response([1, 2, 3]));
    const result = await api.search('foo', 'asc', 0);
    assert.equal(result.length, 3);
  });
});
