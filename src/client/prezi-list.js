import React from 'react';

export default ({prezis, loading, error, hasMore, onLoadMore}) => {
  if (error) {
    return (
      <ul className='prezi-list'>
        <li className='prezi-list--item--loading'>Whoops, an error!</li>
      </ul>
    );
  }

  return (
    <ul className='prezi-list'>
      {
        prezis.map(prezi => (
          <li key={prezi.id} className='prezi-list--item prezi'>
            <div className='prezi--thumbnail'>
              <img src={prezi.thumbnail} alt={prezi.title} />
            </div>
            <div className='prezi--info'>
              <span className='prezi--title'>{prezi.title}</span> by <span className='prezi--creator'>{prezi.creator.name}</span>
            </div>
            <div className='prezi--date'>{prezi.createdAt}</div>
          </li>
        ))
      }
      {loading && <li className='prezi-list--item--loading'>Loading...</li>}
      {hasMore && <li className='prezi-list--item--load-more'><button onClick={onLoadMore}>Load more</button></li>}
    </ul>
  );
};
