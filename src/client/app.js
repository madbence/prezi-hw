import React from 'react';
import ReactDOM from 'react-dom';
import debounce from 'lodash.debounce';

import PreziList from './prezi-list';
import * as api from './api';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      result: [],
      loading: true,
      term: '',
      page: 0,
      order: 'desc',
    };

    this.search = debounce(this.search.bind(this), 300);
  }

  componentDidMount() {
    this.search();
  }

  async search() {
    const token = Date.now();
    this.setState({
      loading: true,
      token,
    });

    try {
      const result = await api.search(this.state.term, this.state.order, this.state.page);
      if (this.state.token !== token) return;
      this.setState({
        loading: false,
        error: null,
        result: this.state.page === 0 ? result : [...this.state.result, ...result],
        hasMore: result.length >= 12,
      });
    } catch (error) {
      this.setState({
        loading: false,
        error,
      });
    }
  }

  toggleOrder() {
    this.setState({order: this.state.order === 'asc' ? 'desc' : 'asc', page: 0});
    this.search();
  }

  updateTerm(term) {
    this.setState({term, page: 0});
    this.search();
  }

  loadMore() {
    this.setState({page: this.state.page + 1});
    this.search();
  }

  render() {
    return (
      <div className='prezi-search'>
        <div className='prezi-search--search'>
          <input placeholder='Search for a prezi' value={this.state.term} onChange={e => this.updateTerm(e.target.value)} />
          <button onClick={() => this.toggleOrder()}>{this.state.order === 'asc' ? 'Oldest first' : 'Newest first'}</button>
        </div>
        <PreziList
          prezis={this.state.result}
          loading={this.state.loading}
          error={this.state.error}
          hasMore={this.state.hasMore}
          onLoadMore={() => this.loadMore()}
        />
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('app'));
