import fetch from './fetch';

export async function search(term, order, page) {
  const res = await fetch(`/search?q=${encodeURIComponent(term)}&order=${order}&page=${page}`, {
    headers: {
      accept: 'application/json',
    },
  });

  if (res.status >= 400) throw new Error('Failed fetch search results');

  return res.json();
}
