if (!process.env.PORT) throw new Error('Whoops, PORT is not set! Maybe you forgot to `source .env`?');


export default {
  port: process.env.PORT,
  env: process.env.NODE_ENV,
};
