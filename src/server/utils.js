import fs from 'fs';
import {promisify} from 'util';
import {get} from 'koa-route';

import config from './config';

const readFile = promisify(fs.readFile);

export const serve = (url, path, type) => {
  if (config.env === 'production') {
    const content = fs.readFileSync(path);
    return get(url, ctx => {
      ctx.body = content;
      ctx.type = type;
    });
  }

  return get(url, async ctx => {
    ctx.body = await readFile(path);
    ctx.type = type;
  });
};
