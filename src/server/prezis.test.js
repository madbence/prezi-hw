/* eslint-env mocha */
import * as prezis from './prezis';
import assert from 'assert';

describe('server/prezis', () => {
  it('should filter by title', async () => {
    const results = await prezis.search('enim', 'asc', 0);
    for (const result of results) assert.ok(result.title.includes('enim'), `${result.title} should contain 'enum'`);
  });
  it('should sort by date', async () => {
    let results = await prezis.search('', 'asc', 0);
    for (let i = 0; i < results.length - 1; i++) {
      assert.ok(
        new Date(results[i].createdAt) <= new Date(results[i + 1].createdAt),
        `${results[i].createdAt} should be before ${results[i + 1].createdAt}`
      );
    }
    results = await prezis.search('', 'desc', 0);
    for (let i = 0; i < results.length - 1; i++) {
      assert.ok(
        new Date(results[i].createdAt) >= new Date(results[i + 1].createdAt),
        `${results[i].createdAt} should be after ${results[i + 1].createdAt}`
      );
    }
  });
  it('should paginate', async () => {
    let results = await prezis.search('enim', 'asc', 0);
    assert.equal(results.length, 3);
    results = await prezis.search('enim', 'asc', 1);
    assert.equal(results.length, 0);
  });
});
