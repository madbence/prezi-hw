import Koa from 'koa';
import {get} from 'koa-route';

import {serve} from './utils';
import config from './config';
import * as prezis from './prezis';

const app = new Koa();

app

  .use(serve('/', './dist/index.html', 'text/html'))
  .use(serve('/bundle.js', './dist/bundle.js', 'application/javascript'))
  .use(serve('/bundle.css', './dist/bundle.css', 'text/css'))

  .use(get('/search', async ctx => {
    const substr = ctx.query.q || '';
    const order = ctx.query.order === 'asc' ? 'asc' : 'desc';
    const page = +ctx.query.page || 0;

    ctx.body = await prezis.search(substr, order, page);
  }));

app.listen(config.port);
