// @flow
import db from '../../prezis.json';

type Prezi = {
  id: string,
  title: string,
  thumbnail: string,
  creator: {
    name: string,
    profileUrl: string,
  },
  createdAt: string,
};

const PAGE_SIZE = 12;

const byDate = order => order === 'asc'
  ? (a, b) => new Date(a.createdAt) - new Date(b.createdAt)
  : (a, b) => new Date(b.createdAt) - new Date(a.createdAt);

const byTitle = substr => prezi => prezi.title.includes(substr);

// eslint-disable-next-line require-await
export async function search(substr: string, order: 'asc' | 'desc', page: number): Promise<Prezi[]> {
  return db
    .filter(byTitle(substr))
    .sort(byDate(order))
    .slice(page * PAGE_SIZE, (page + 1) * PAGE_SIZE);
}
