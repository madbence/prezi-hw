# prezi homework [![pipeline status](https://gitlab.com/prezi-homeassignments/Bence.Danyi-fullstack/badges/master/pipeline.svg)](https://gitlab.com/prezi-homeassignments/Bence.Danyi-fullstack/commits/master) [![coverage report](https://gitlab.com/prezi-homeassignments/Bence.Danyi-fullstack/badges/master/coverage.svg)](https://gitlab.com/prezi-homeassignments/Bence.Danyi-fullstack/commits/master)

## requirements

- `node@8.7.0`
- `npm@5.4.2`
- `make`

## usage

```sh
$ npm i         # install dependencies
$ source .env   # set up environment, based on .env.example
$ make watch    # start dev server
$ make format   # fix lint errors (some of them)
$ make test     # run tests (typechecker, linter, coverage)
$ make e2e      # run e2e tests (assumes that the dev server is running)
$ make build    # build prod assets
$ make deploy   # deploy HEAD to heroku
```

## demo

https://prezi-bd-fullstack.herokuapp.com/
